using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomManagerScript : MonoBehaviour
{
    private PlayerStatsScript playerStatsScript;

    [SerializeField] private GameObject[] availableEnemies;
    [SerializeField] private GameObject enemySpawnEffect;

    private GameObject player;
    private Vector2[] availablePlayerPositions =
        {
            new Vector2(0, 3.75f),
            new Vector2(3.75f, 0),
            new Vector2(0, -3.75f),
            new Vector2(-3.75f, 0)
        };
    private float roomBorder = 3.5f;
    private float minDistanceToPlayerOnSpawn = 4f;


    [SerializeField] private GameObject[] allDoors;
    private List<GameObject> openDoors = new List<GameObject>();

    private void Start()
    {
        playerStatsScript = GameObject.FindObjectOfType<PlayerStatsScript>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player"))
            return;
        SetPlayerPosition(collision.gameObject);
        StartFightMode();
    }

    private void StartFightMode()
    {
        int numberOfEnemies = Random.Range(3, 6);
        for (int i = 0; i < numberOfEnemies; i++)
        {
            SpawnEnemy();
        }
        
        CloseDoors();
    }

    private void SetPlayerPosition(GameObject player)
    {
        Vector3 playerRelativePosition = player.transform.position - transform.position;
        Vector3 leaderPoint = Vector3.positiveInfinity;

        foreach (Vector2 availablePlayerPosition in availablePlayerPositions)
        {
            float playerDistanceToPoint = Vector2.Distance(playerRelativePosition, availablePlayerPosition);
            float playerDistanceToLeaderPoint = Vector2.Distance(playerRelativePosition, leaderPoint);
            if (playerDistanceToPoint < playerDistanceToLeaderPoint)
                leaderPoint = availablePlayerPosition;
        }

        player.transform.position = transform.position + leaderPoint;
    }

    private void CloseDoors()
    {
        for (int i = 0; i < allDoors.Length; i++)
        {
            if (allDoors[i].activeSelf == false)
            {
                openDoors.Add(allDoors[i].gameObject);
                allDoors[i].SetActive(true);
            }
        }
    }

    private void SpawnEnemy()
    {
        int enemyToSpawn = Random.Range(0, availableEnemies.Length);
        Vector3 spawnPosition;
        float distanceToPlayer;
        do
        {
            float offsetX = Random.Range(-roomBorder, roomBorder);
            float offsetY = Random.Range(-roomBorder, roomBorder);
            spawnPosition = new Vector3(transform.position.x + offsetX, transform.position.y + offsetY, 0);

            distanceToPlayer = Vector2.Distance(spawnPosition, player.transform.position);
        } while (distanceToPlayer < minDistanceToPlayerOnSpawn);
        
        
        Instantiate(enemySpawnEffect, spawnPosition, Quaternion.identity);
        GameObject spawnedEnemy = Instantiate(availableEnemies[enemyToSpawn], spawnPosition, Quaternion.identity);
        spawnedEnemy.GetComponent<EnemyScript>().parentRoom = this.gameObject;
    }

    public void UpdateFightStatus()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        int enemiesCount = enemies.Length;
        if (enemiesCount > 0)
            return;
        StartCoroutine(EndFightMode());
    }

    private IEnumerator EndFightMode()
    {
        if (playerStatsScript.artifactData.healthRegenerationEnabled)
            GameObject.FindObjectOfType<PlayerHealthScript>().Heal(1);

        for (int i = 0; i < openDoors.Count; i++)
        {
            openDoors[i].GetComponent<Animator>().SetTrigger("StartOpenDoorAnimation");
        }

        yield return new WaitForSeconds(0.5f);

        for (int i = 0; i < openDoors.Count; i++)
        {
            openDoors[i].SetActive(false);
        }

        Destroy(this);
    }

}
