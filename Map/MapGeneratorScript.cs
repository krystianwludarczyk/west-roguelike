using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MapGeneratorScript : MonoBehaviour
{
    [SerializeField] private GameObject roomObject;
    [SerializeField] private float mapBorder = 2.5f;

    [Header("Decorations")]
    [SerializeField] private int numberOfDecorations = 12;
    [SerializeField] private GameObject[] availableDecorations;
    [SerializeField] private int numberOfKegs;
    [SerializeField] private GameObject kegObject;

    private List<Vector3> spawnedDecorationsPositions = new List<Vector3>();

    //Variables
    private List<GameObject> spawnedRooms = new List<GameObject>();
    private Vector3[] spawnRoomOffsets = {Vector3.up * 10f, Vector3.right * 10f, Vector3.down * 10f, Vector3.left * 10f};

    //Settings
    private int mapSize = 25;

    private void Start()
    {
        GenerateMap();
    }
    
    //Generate functions
    private void GenerateMap()
    {
        spawnedRooms.Add(this.gameObject);
        //SetRoomType(spawnedRooms[spawnedRooms.Count - 1], "start");

        for (int i = 0; i < mapSize; i++)
        {
            GenerateRoom();
        }
        // SetRoomType(spawnedRooms[spawnedRooms.Count - 1], "end");

        for (int i = 0; i < spawnedRooms.Count; i++)
        {
            OpenRoom(spawnedRooms[i]);

        }
        Destroy(this);
    }

    private void GenerateRoom()
    {
        GameObject parentRoom = spawnedRooms[spawnedRooms.Count - 1];
        List<Vector3> availablePositions = GetAvilableSpawnPositions(parentRoom);
        while (availablePositions.Count <= 0)
        {
            parentRoom = spawnedRooms[Random.Range(0, spawnedRooms.Count - 1)];
            availablePositions = GetAvilableSpawnPositions(parentRoom);
        } 
        Vector3 newRoomPosition = availablePositions[Random.Range(0, availablePositions.Count)];
        GameObject spawnedRoom = Instantiate(roomObject, newRoomPosition, transform.rotation);
        spawnedRooms.Add(spawnedRoom);
        GenerateRoomEnviroment(spawnedRoom);
    }

    private void GenerateRoomEnviroment(GameObject parentRoom)
    {
        for (int i = 0; i < numberOfKegs; i++)
        {
            Vector3 barrelSpawnPosition = GetBarrelSpawnPosition(parentRoom.transform.position);
            GameObject spanedObstacleDecoration = Instantiate(kegObject, barrelSpawnPosition, Quaternion.identity);
            spanedObstacleDecoration.GetComponent<SpriteRenderer>().flipX = Random.value > 0.5f;
            spawnedDecorationsPositions.Add(barrelSpawnPosition);
        }

        for (int i = 0; i < numberOfDecorations; i++)
        {
            Vector3 spawnPosition = GetDecorationSpawnPosition(parentRoom.transform.position);
            int nonObstacleToSpawn = Random.Range(0, availableDecorations.Length);
            GameObject spanedNonObstacleDecoration = Instantiate(availableDecorations[nonObstacleToSpawn], spawnPosition, Quaternion.identity);
            spanedNonObstacleDecoration.GetComponent<SpriteRenderer>().flipX = Random.value > 0.5f;
            spawnedDecorationsPositions.Add(spawnPosition);
        }

    }

    private Vector3 GetBarrelSpawnPosition(Vector3 parentRoomPosition)
    {
        Vector3 spawnPosition = Vector3.zero;
        bool isPositionUnique = false;
        Vector3[] availableBarrelSpawnPositions =
        {
            new Vector3(-2.5f, -0.5f, 0),
            new Vector3(-2.5f, 0.5f, 0),
            new Vector3(-0.5f, 2.5f, 0),
            new Vector3(0.5f, 2.5f, 0),
            new Vector3(2.5f, 0.5f, 0),
            new Vector3(2.5f, -0.5f, 0),
            new Vector3(0.5f, -2.5f, 0),
            new Vector3(-0.5f, -2.5f, 0),
        };
        do
        {
            Vector3 offset = availableBarrelSpawnPositions[Random.Range(0, availableBarrelSpawnPositions.Length)];
            spawnPosition = parentRoomPosition + offset;
            isPositionUnique = !spawnedDecorationsPositions.Any(spawnedDecorationsPosition => spawnedDecorationsPosition == spawnPosition);
        }
        while (!isPositionUnique);
        return spawnPosition;
    }

    private Vector3 GetDecorationSpawnPosition(Vector3 parentRoomPosition)
    {
        Vector3 spawnPosition = Vector3.zero;
        bool isPositionUnique = false;
        do
        {
            float spawnPositionX = Mathf.Round(Random.Range(-mapBorder, mapBorder) * 2.0f) / 2.0f;
            if (spawnPositionX % 1 == 0)
                spawnPositionX += 0.5f;

            float spawnPositionY = Mathf.Round(Random.Range(-mapBorder, mapBorder) * 2.0f) / 2.0f;
            if (spawnPositionY % 1 == 0)
                spawnPositionY += 0.5f;

            Vector3 offset = new Vector3(spawnPositionX, spawnPositionY, 0);
            spawnPosition = parentRoomPosition + offset;
            isPositionUnique = !spawnedDecorationsPositions.Any(spawnedDecorationsPosition => spawnedDecorationsPosition == spawnPosition);
        }
        while (!isPositionUnique);
        return spawnPosition;
    }


    private List<Vector3> GetAvilableSpawnPositions(GameObject parentRoom)
    {
        Vector3 parentRoomPosition = parentRoom.transform.position;
        List<Vector3> availablePositions = new List<Vector3>();

        for (int i = 0; i < spawnRoomOffsets.Length; i++)
        {
            Vector3 offset = spawnRoomOffsets[i];
            Vector3 tryRoomPosition = parentRoomPosition + offset;
            bool roomExists = spawnedRooms.Any(room => room.transform.position == tryRoomPosition);
            if (!roomExists)
                availablePositions.Add(tryRoomPosition);
        }

        return availablePositions;
    }

    private void OpenRoom(GameObject room, int? doorID = null)
    {
        if (doorID.HasValue)
            room.transform.GetChild(doorID.Value).gameObject.SetActive(false);
        else
        {
            Vector3 roomPosition = room.transform.position;
            for (int i = 0; i < spawnRoomOffsets.Length; i++)
            {
                Vector3 offset = spawnRoomOffsets[i];
                Vector3 pointPosition = roomPosition + offset;

                bool isPointOccupied = spawnedRooms.Any(room => room.transform.position == pointPosition);
                if (isPointOccupied)
                    room.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }
    private void SetRoomType(GameObject room, string type)
    {
        if (type == "start")
            room.GetComponent<SpriteRenderer>().color = Color.green;
        else if (type == "special")
            room.GetComponent<SpriteRenderer>().color = Color.yellow;
        else if (type == "end")
            room.GetComponent<SpriteRenderer>().color = Color.red;
    }
}
