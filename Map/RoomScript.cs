using UnityEngine;

public class RoomScript : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player"))
            return;
        CameraScript cameraScript = GameObject.FindObjectOfType<CameraScript>();
        cameraScript.SetCameraTarget(this.transform);
    }
}
