using UnityEngine;

public class PickupArtifactScript : MonoBehaviour
{
    [SerializeField] private GameObject pickupIcon;
    private Transform playerTransform;
    private bool canPickup;

    [SerializeField] private ArtifactData[] availableArtifactData;
    [SerializeField] private ArtifactData artifactData;

    private void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        if (!artifactData)
            SetArtifactData(availableArtifactData[Random.Range(0, availableArtifactData.Length)]);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player"))
            return;
        canPickup = true;
        pickupIcon.SetActive(true);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player"))
            return;
        canPickup = false;
        pickupIcon.SetActive(false);
    }

    private void Update()
    {
        if (!canPickup)
            return;
        pickupIcon.transform.position = playerTransform.position + new Vector3(0, 1.25f, 0);
        if (Input.GetKeyDown(KeyCode.E))
            PickupArtifact();
    }

    private void PickupArtifact()
    {
        PlayerStatsScript playerStatsScript = GameObject.FindObjectOfType<PlayerStatsScript>();
        ArtifactData newArtifactData = playerStatsScript.GetArtifactData();
        playerStatsScript.SetArtifactData(artifactData);
        if (newArtifactData.sprite)
            SetArtifactData(newArtifactData);
        else
            Destroy(gameObject);
    }

    private void SetArtifactData(ArtifactData newArtifactData)
    {
        artifactData = newArtifactData;
        GetComponent<SpriteRenderer>().sprite = newArtifactData.sprite;
    }
}
