# West Roguelike
Here's the source code for my Unity2D game.
West Roguelike was developed for about 2 weeks. I used Unity2D to create the game, and the code was written in C# using Visual Studio.

[play demo](https://krystianwludarczyk.itch.io/unity-c-west-roguelike-prototype)
