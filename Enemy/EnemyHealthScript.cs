using System.Collections;
using UnityEngine;

public class EnemyHealthScript : MonoBehaviour
{
    [SerializeField] private int health;
    [SerializeField] private GameObject deathEffect;
    [SerializeField] private Material flashMaterial;

    [Header("Optional")]
    [SerializeField] private GameObject[] availableLoot;
    
    private SpriteRenderer spriteRenderer;
    private Material originalMaterial;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        originalMaterial = spriteRenderer.material;
    }

    public void DealDamage(int damage, GameObject source)
    {
        health -= damage;

        if (health <= 0)
            KillEnemy(source);
        else
            StartCoroutine(FlashRoutine());
    }

    private IEnumerator FlashRoutine()
    {
        spriteRenderer.material = flashMaterial;
        yield return new WaitForSeconds(0.05f);
        spriteRenderer.material = originalMaterial;
    }

    private void KillEnemy(GameObject source)
    {
        if (deathEffect)
        {
            Vector2 direction = (source.transform.position - transform.position).normalized;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            Quaternion targetRotation = Quaternion.Euler(0, 0, angle - 180);
            GameObject spawnedDeathEffect = Instantiate(deathEffect, transform.position, targetRotation);

            DeathEffectScript deathEffectScript = spawnedDeathEffect.GetComponent<DeathEffectScript>();
            if (deathEffectScript)
                deathEffectScript.bodySpriteRenderer.sprite = this.gameObject.GetComponent<SpriteRenderer>().sprite;
        }

        if (availableLoot.Length > 0)
        {
            int lootToSpawn = Random.Range(0, availableLoot.Length);
            Instantiate(availableLoot[lootToSpawn], transform.position, Quaternion.identity);
        }

        Destroy(gameObject);
    }

}
