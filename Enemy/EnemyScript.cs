using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    [Header("Movement")]   
    [SerializeField] private movementOption movementType;
    private enum movementOption {toPlayer, random};
    [SerializeField] private float movementSpeed;

    //if moverandom movement
    [SerializeField] private float minDistanceToPlayer, maxDistanceToPlayer;

    private SpriteRenderer spriteRenderer;
    private Transform playerTransform;

    //random movement
    [HideInInspector] public GameObject parentRoom;
    private float roomBorder = 3.5f;
    private Vector3 movementTarget;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        if (movementType == movementOption.random)
            SetNewMovementTarget();
    }

    private void FixedUpdate()
    {
        MoveEnemy();
    }

    private void MoveEnemy()
    {
        if (movementType == movementOption.toPlayer)
            movementTarget = playerTransform.position;

        transform.position = Vector2.MoveTowards(transform.position, movementTarget, movementSpeed * Time.deltaTime);
        spriteRenderer.flipX = transform.position.x - playerTransform.position.x > 0;
    }

    private void SetNewMovementTarget()
    {
        Vector3 newMovementTarget;
        float distanceToPlayer;
        do
        {
            float offsetX = Random.Range(-roomBorder, roomBorder);
            float offsetY = Random.Range(-roomBorder, roomBorder);
            newMovementTarget = new Vector3(parentRoom.transform.position.x + offsetX, parentRoom.transform.position.y + offsetY, 0);
            distanceToPlayer = Vector2.Distance(newMovementTarget, playerTransform.position);
        } while (distanceToPlayer > maxDistanceToPlayer || distanceToPlayer < minDistanceToPlayer);
        
        movementTarget = newMovementTarget;
        Invoke(nameof(SetNewMovementTarget),0.75f);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            collision.gameObject.GetComponent<PlayerHealthScript>().DealDamage(1);
    }

    private void OnDestroy()
    {
        if (parentRoom)
            parentRoom.GetComponent<RoomManagerScript>().UpdateFightStatus();
    }
}
