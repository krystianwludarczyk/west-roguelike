using UnityEngine;

public class EnemyGunScript : MonoBehaviour
{
    [SerializeField] private GameObject bullet;
    [SerializeField] private float bulletForce;
    [SerializeField] private int bulletDamage;
    [SerializeField] private float bulletCountPerShot;
    [SerializeField] private float timeBetweenShoot;

    private float cooldownTimer;
    private Transform playerTransform;

    private void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        cooldownTimer = Random.Range(-timeBetweenShoot, 0);
    }

    private void Update()
    {
        RotateGun();

        cooldownTimer += Time.deltaTime;
        if (cooldownTimer > timeBetweenShoot)
            Shoot();
    }

    private void RotateGun()
    {
        Vector3 direction = playerTransform.position - transform.position;

        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion targetRotation = Quaternion.Euler(0, 0, angle);
        transform.rotation = targetRotation;
        GetComponent<SpriteRenderer>().flipY = playerTransform.position.x < transform.position.x;
    }

    private void Shoot()
    {
        cooldownTimer = Random.Range(-0.5f, 0.5f);

        for (int i = 1; i <= bulletCountPerShot; i++)
        {
            GameObject spawnedBullet = Instantiate(bullet, GetBulletSpawnPosition(), Quaternion.identity);

            float gunRotationZ = transform.eulerAngles.z;
            float angleOffset;
            if (bulletCountPerShot % 2 == 0)
                angleOffset = 10f * Mathf.CeilToInt(i / 2f) - 5f;
            else
                angleOffset = 10f * Mathf.Floor(i / 2f);
            angleOffset *= (i % 2 == 0 ? 1 : -1);
            gunRotationZ += angleOffset;

            float angleRadians = gunRotationZ * Mathf.Deg2Rad;
            Vector3 direction = new Vector3(Mathf.Cos(angleRadians), Mathf.Sin(angleRadians), 0f).normalized;
            spawnedBullet.GetComponent<EnemyBulletScript>().LaunchBullet(gameObject ,direction, bulletDamage);
        }
    }

    private Vector3 GetBulletSpawnPosition()
    {
        Vector3 position = transform.position;
        Vector3 offset;
        Vector3 offsetX = new Vector3(0.25f, 0f, 0f);
        Vector3 offsetY = new Vector3(0f, 0.25f, 0f);

        offset = Quaternion.Euler(0f, 0f, transform.eulerAngles.z) * offsetX;
        position += offset - offsetY;
        return position;
    }
}
