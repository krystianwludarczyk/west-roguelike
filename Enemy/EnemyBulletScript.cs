using UnityEngine;

public class EnemyBulletScript : MonoBehaviour
{
    private int damage;
    private GameObject parentGun;
    private int hitTimes;

    private void Start()
    {
        Destroy(this.gameObject, 2f);
    }

    public void LaunchBullet(GameObject parentGun, Vector3 direction, int damage)
    {
        GetComponent<Rigidbody2D>().AddForce(direction * 500f);
        this.damage = damage;
        this.parentGun = parentGun;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (hitTimes > 0 || (!collision.CompareTag("Player") && !collision.CompareTag("Obstacle")))
            return;

        hitTimes++;
        if (collision.CompareTag("Player"))
        {
            PlayerHealthScript playerHealthScript = collision.GetComponent<PlayerHealthScript>();
            if (playerHealthScript)
                playerHealthScript.DealDamage(damage);
        }
        else if (collision.CompareTag("Obstacle"))
        {
            DurabilityScript durabilityScript = collision.GetComponent<DurabilityScript>();
            if (durabilityScript)
                durabilityScript.DealDamage(damage, parentGun);
        }
        Destroy(gameObject);
    }
}
