using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Unity.VisualScripting.Member;

public class DurabilityScript : MonoBehaviour
{
    [SerializeField] private bool godMode;
    [SerializeField] private int health;
    [SerializeField] private Material flashMaterial;

    [Header("Optional")]
    [SerializeField] private GameObject deathEffect;
    [SerializeField] private float chanceToDrop;
    [SerializeField] private GameObject[] availableDrop;

    private SpriteRenderer spriteRenderer;
    private Material originalMaterial;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        originalMaterial = spriteRenderer.material;
    }   

    public void DealDamage(int damage, GameObject source)
    {
        if (!godMode)
        {
            health -= damage;
            if (health <= 0)
                KillObstacle(source.transform.position);
            else
                StartCoroutine(FlashRoutine());
        }
        else
            StartCoroutine(FlashRoutine());
    }

    private IEnumerator FlashRoutine()
    {
        spriteRenderer.material = flashMaterial;
        yield return new WaitForSeconds(0.05f);
        spriteRenderer.material = originalMaterial;
    }

    private void KillObstacle(Vector3 sourcePosition)
    {
        if (deathEffect)
        {
            Vector2 direction = (sourcePosition - transform.position).normalized;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            Quaternion targetRotation = Quaternion.Euler(0, 0, angle - 180);
            Instantiate(deathEffect, transform.position, targetRotation);
        }

        float randomValue = Random.value;
        if (randomValue < chanceToDrop)
            Instantiate(availableDrop[Random.Range(0, availableDrop.Length)], transform.position, Quaternion.identity);

        Destroy(gameObject);
    }
}
