using System.Collections;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField] private GameObject vision;
    [SerializeField] private GameObject crosshair;
    [SerializeField] private GameObject inGameUI;
    [SerializeField] private GameObject pressSpaceToStart;

    private float movementSpeed = 7.5f;
    private Vector3 positionOffset = new Vector3(0, 0, -10);

    private Vector3 originalPosition;
    private bool isCameraShaking;

    private Transform cameraTarget;

    private void Start()
    {
        vision.SetActive(true);
    }

    private void Update()
    {
        MoveCamera();
        MoveCrosshair();
    }


    private void MoveCamera()
    {
        if (!cameraTarget || transform.position == cameraTarget.position + positionOffset)
            return;

        if (Vector2.Distance(transform.position, cameraTarget.position + positionOffset) < 0.05f)
            transform.position = cameraTarget.position + positionOffset;
        else
            transform.position = Vector3.Lerp(transform.position, cameraTarget.position + positionOffset, Time.deltaTime * movementSpeed);
    }

    private void MoveCrosshair()
    {
        if (Cursor.visible == true)
            Cursor.visible = false;

        Vector3 mousePosition = Input.mousePosition;
        Vector3 targetPosition = new Vector3(mousePosition.x, mousePosition.y, 10);
        crosshair.transform.position = Camera.main.ScreenToWorldPoint(targetPosition);
    }

    public void SetCameraTarget(Transform newCameraTarget)
    {
        cameraTarget = newCameraTarget;
        vision.transform.position = cameraTarget.position;
    }

    public void StartEndAnimation()
    {
        inGameUI.GetComponent<Animator>().SetTrigger("startEndAnimation");
        pressSpaceToStart.SetActive(true);
    }

    public void ShakeCamera(int shakeForce)
    {
        if (!isCameraShaking)
            StartCoroutine(Shake(shakeForce));
    }

    private IEnumerator Shake(int shakeForce)
    {
        float shakeDuration = 0.05f * shakeForce;
        float shakeMagnitude = 0.25f * shakeForce/2;

        isCameraShaking = true;
        originalPosition = transform.localPosition;
        float elapsed = 0f;
        while (elapsed < shakeDuration)
        {
            float x = originalPosition.x + Random.Range(-1f, 1f) * shakeMagnitude;
            float y = originalPosition.y + Random.Range(-1f, 1f) * shakeMagnitude;

            transform.localPosition = new Vector3(x, y, originalPosition.z);
            elapsed += Time.deltaTime;
            yield return null;
        }

        transform.localPosition = originalPosition;
        isCameraShaking = false;
    }


}
