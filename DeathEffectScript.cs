using UnityEngine;

public class DeathEffectScript : MonoBehaviour
{
    public SpriteRenderer bodySpriteRenderer;

    private Color startingColor;
    private float timeElapsed = 0f;
    private float duration = 2f;

    private void Start()
    {
        startingColor = bodySpriteRenderer.color;

        Destroy(bodySpriteRenderer.gameObject, 2f);
        Destroy(this, 2f);
    }

    private void Update()
    {
        timeElapsed += Time.deltaTime;
        float newAlpha = Mathf.Lerp(startingColor.a, startingColor.a - 255f / 255f, timeElapsed / duration);
        bodySpriteRenderer.color = new Color(startingColor.r, startingColor.g, startingColor.b, newAlpha);
    }
}
