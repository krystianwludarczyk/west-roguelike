using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour
{
    private bool isGameEnded;

    public void EndGame()
    {
        StartCoroutine(EndGameRoutine());
    }

    private IEnumerator EndGameRoutine()
    {
        GameObject.FindObjectOfType<CameraScript>().StartEndAnimation();
        yield return new WaitForSeconds(0.5f);
        isGameEnded = true;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isGameEnded)
            RestartGame();
    }

    private void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
