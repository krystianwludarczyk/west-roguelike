using UnityEngine;

public class PlayerMovementScript : MonoBehaviour
{
    private PlayerStatsScript playerStatsScript;

    private float movementSpeed = 4f;
    private SpriteRenderer spriteRenderer;
    private Rigidbody2D rb2D;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rb2D = GetComponent<Rigidbody2D>();

        playerStatsScript = GameObject.FindObjectOfType<PlayerStatsScript>();
    }

    private void FixedUpdate()
    {
        MovePlayer();
    }

    private void MovePlayer()
    {
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float verticalInput = Input.GetAxisRaw("Vertical");

        Vector2 direction = new Vector2(horizontalInput, verticalInput).normalized;
        rb2D.velocity = direction * movementSpeed * playerStatsScript.artifactData.movementSpeedMultiplier;

        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        spriteRenderer.flipX = mousePosition.x - transform.position.x < 0;
    }
}
