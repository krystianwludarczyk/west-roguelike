using UnityEngine;

public class PlayerStatsScript : MonoBehaviour
{
    public ArtifactData artifactData;
    [SerializeField] private SpriteRenderer artifactSpriteDisplay;
    [SerializeField] private SpriteRenderer artifactInfoSpriteDisplay;
    public GunData gunData;
    

    public bool godMode;
    public int maxHealth;
    public int health;

    public void SetGunData(GunData newGunData)
    {
        gunData = newGunData;
        FindObjectOfType<PlayerGunScript>().GetComponent<SpriteRenderer>().sprite = newGunData.sprite;
    }
    public GunData GetGunData() => gunData;

    public void SetArtifactData(ArtifactData newArtifactData)
    {
        artifactData = newArtifactData;
        artifactSpriteDisplay.sprite = newArtifactData.sprite;
        artifactInfoSpriteDisplay.sprite = newArtifactData.infoSprite;
    }
    public ArtifactData GetArtifactData() => artifactData;
}



