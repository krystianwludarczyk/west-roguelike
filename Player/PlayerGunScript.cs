using UnityEngine;

public class PlayerGunScript : MonoBehaviour
{
    private PlayerStatsScript playerStatsScript;

    private GameObject mainCamera;
    private float cooldownTimer;

    private void Start()
    {
        playerStatsScript = GameObject.FindObjectOfType<PlayerStatsScript>();
        mainCamera = GameObject.FindObjectOfType<Camera>().gameObject;
    }

    private void Update()
    {
        RotateGun();
        cooldownTimer += Time.deltaTime;
        if (Input.GetMouseButton(0) && cooldownTimer > playerStatsScript.GetGunData().cooldown * playerStatsScript.artifactData.reloadTimeMultiplier) 
            Shoot();
    }

    private void RotateGun()
    {
        Vector3 mouseInput = Input.mousePosition;
        Vector3 mousePosition = mainCamera.GetComponent<Camera>().ScreenToWorldPoint(mouseInput);
        Vector3 direction = mousePosition - transform.position;

        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion targetRotation = Quaternion.Euler(0, 0, angle);
        transform.rotation = targetRotation;
        GetComponent<SpriteRenderer>().flipY = mousePosition.x < transform.position.x;
    }
    
    private void Shoot()
    {
        cooldownTimer = 0;

        mainCamera.GetComponent<CameraScript>().ShakeCamera(1);

        for (int i = 1; i <= playerStatsScript.GetGunData().bulletCountPerShot; i++)
        {
            GameObject spawnedBullet = Instantiate(playerStatsScript.GetGunData().bullet, GetBulletSpawnPosition(), Quaternion.identity);

            float gunRotationZ = transform.eulerAngles.z;
            float angleOffset;
            if (playerStatsScript.GetGunData().bulletCountPerShot % 2 == 0)
                angleOffset = 10f * Mathf.CeilToInt(i / 2f) - 5f;
            else
                angleOffset = 10f * Mathf.Floor(i / 2f);
            angleOffset *= (i % 2 == 0 ? 1 : -1);
            gunRotationZ += angleOffset;

            float angleRadians = gunRotationZ * Mathf.Deg2Rad;
            Vector3 direction = new Vector3(Mathf.Cos(angleRadians), Mathf.Sin(angleRadians), 0f).normalized;
            spawnedBullet.GetComponent<PlayerBulletScript>().LaunchBullet(gameObject, direction, playerStatsScript.GetGunData().bulletDamage);
        }
    }

    private Vector3 GetBulletSpawnPosition()
    {
        Vector3 position = transform.position;
        Vector3 offset;
        Vector3 offsetX = new Vector3(0.25f, 0f, 0f);
        Vector3 offsetY = new Vector3(0f, 0.25f, 0f);

        offset = Quaternion.Euler(0f, 0f, transform.eulerAngles.z) * offsetX;
        position += offset - offsetY;
        return position;
    }
}
