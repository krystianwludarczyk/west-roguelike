using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerHealthScript : MonoBehaviour
{
    private PlayerStatsScript playerStatsScript;
    [SerializeField] private Material flashMaterial;

    [Header("Display")]
    [SerializeField] private SpriteRenderer[] heartSpriteRenderers;
    [SerializeField] private Sprite[] heartSprites;

    private SpriteRenderer spriteRenderer;
    private Material originalMaterial;

    private void Start()
    {
        playerStatsScript = GameObject.FindObjectOfType<PlayerStatsScript>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        originalMaterial = spriteRenderer.material;
    }

    public void Heal(int value)
    {
        if (playerStatsScript.health >= playerStatsScript.maxHealth)
            return;
        playerStatsScript.health += value;
        UpdateUI();
    }

    public void DealDamage(int value)
    {
        if (playerStatsScript.godMode)
            return;

        playerStatsScript.health -= value;
        UpdateUI();
        if (playerStatsScript.health <= 0)
            KillPlayer();
        else
            StartCoroutine(FlashRoutine());
    }

    private void UpdateUI()
    {
        for (int i = 0; i < heartSpriteRenderers.Length; i++)
        {
            if (i <= playerStatsScript.health - 1)
                heartSpriteRenderers[i].sprite = heartSprites[1];
            else
                heartSpriteRenderers[i].sprite = heartSprites[0];
        }
    }

    private IEnumerator FlashRoutine()
    {
        for (int i = 0; i < 2; i++)
        {
            spriteRenderer.material = flashMaterial;
            yield return new WaitForSeconds(0.25f);
            spriteRenderer.material = originalMaterial;
            yield return new WaitForSeconds(0.25f);
        }
    }

    private void KillPlayer()
    {
        GameObject.FindObjectOfType<GameManagerScript>().EndGame();
    }
}
