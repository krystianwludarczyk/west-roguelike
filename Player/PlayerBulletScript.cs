using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBulletScript : MonoBehaviour
{
    private int damage;
    private GameObject parentGun;
    private int hitTimes;

    private void Start()
    {
        Destroy(this.gameObject, 2f);
    }

    public void LaunchBullet(GameObject parentGun, Vector3 direction, int damage)
    {
        GetComponent<Rigidbody2D>().AddForce(direction * 1000f);
        this.damage = damage;
        this.parentGun = parentGun;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (hitTimes > 0 || (!collision.CompareTag("Enemy") && !collision.CompareTag("Obstacle")))
            return;

        hitTimes++;
        if (collision.CompareTag("Enemy"))
        {
            EnemyHealthScript enemyHealthScript = collision.GetComponent<EnemyHealthScript>();
            if (enemyHealthScript)
                enemyHealthScript.DealDamage(damage, parentGun);
        }
        else if (collision.CompareTag("Obstacle"))
        {
            DurabilityScript durabilityScript = collision.GetComponent<DurabilityScript>();
            if (durabilityScript)
                durabilityScript.DealDamage(damage, parentGun);
        }
        Destroy(gameObject);
    }

}
