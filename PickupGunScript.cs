using UnityEngine;

public class PickupGunScript : MonoBehaviour
{
    [SerializeField] private GameObject pickupIcon;
    private Transform playerTransform;
    private bool canPickup;

    [SerializeField] private GunData[] availableGunData;
    [SerializeField] private GunData gunData;

    private void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        if (!gunData)
            SetGunData(availableGunData[Random.Range(0, availableGunData.Length)]);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player"))
            return;
        canPickup = true;
        pickupIcon.SetActive(true);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player"))
            return;
        canPickup = false;
        pickupIcon.SetActive(false);
    }

    private void Update()
    {
        if (!canPickup)
            return;
        pickupIcon.transform.position = playerTransform.position + new Vector3(0, 1.25f, 0);
        if (Input.GetKeyDown(KeyCode.E))
            PickupGun();
    }

    private void PickupGun()
    {
        PlayerStatsScript playerStatsScript = GameObject.FindObjectOfType<PlayerStatsScript>();
        GunData newGunData = playerStatsScript.GetGunData();
        playerStatsScript.SetGunData(gunData);
        SetGunData(newGunData);
    }

    private void SetGunData(GunData newGunData)
    {
        gunData = newGunData;
        GetComponent<SpriteRenderer>().sprite = newGunData.sprite;
    }
}
