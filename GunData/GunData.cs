using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "GameData/GunData", order = 1)]
public class GunData : ScriptableObject
{
    public Sprite sprite;
    public float cooldown;
    public int bulletCountPerShot;
    public GameObject bullet;
    public float bulletForce = 1000f;
    public int bulletDamage = 1;
}