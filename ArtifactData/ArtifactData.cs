using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "GameData/ArtifactsData", order = 1)]
public class ArtifactData : ScriptableObject
{
    public Sprite sprite;
    public Sprite infoSprite;

    public bool healthRegenerationEnabled;
    public float movementSpeedMultiplier;
    public float reloadTimeMultiplier;
}
